/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var rect_width_two_columns = 5;
var rect_width_one_column  = 10;
var old_id_selected        = undefined;
var pin_radius             = 50;
var pins_array;
var pins_array_backup;

$( document ).ready(function() {
    
    Generate_Pin_Array();
    
    $("input[name = number_of_columns]").change(function(){
        Change_Number_Of_Columns(this.value);
    });
    
    $("#Pin_Numbers").change(function (){        
       Generate_Pin_Array();
    });
    
    
    $("#btn_Manage_Pins").click(function (){
        $(".btn").prop("disabled",true);
        Show_Window_Manage_Pins("show"); 
    });
    
    $("#exit_window_change_pin").click(function (){
        $(".btn").prop("disabled",false);
        Show_Window_Manage_Pins("hide"); 
    });
    
    $("#exit_icon_change_pin_settings").click(function (){
        Exit_Window_Change_Pin_Settings();
    });
    
    $("#left_icon_change_pin_settings").click(function (){
       Decrease_Pin_ID_Settings(); 
    });
    
    $("#right_icon_change_pin_settings").click(function (){
       Increase_Pin_ID_Settings(); 
    });
    
    $("#setting_pin_name").change(function (){
        var id_selected_pin = $("#pin_name").html();
        Check_Pin_Name_Unicity(id_selected_pin); 
    });
    
    $("#btn_Save_Pin_Settings").click(function (){
        var id_selected_pin = $("#pin_name").html();
        Save_Pin_Settings(id_selected_pin);
    });
    
});

function Show_Window_Manage_Pins(what){
    if(what == "show"){
        $(".Modal_Windows").css("visibility","hidden");
        $("#Window_Manage_Pins").css("visibility","visible");
        Draw_Pin_Table();
    }
    else{
        $("#Window_Manage_Pins").css("visibility","hidden");
        Draw_Symbol_Rectangle();
    }
}

function Draw_Pin_Table(){
    var number_of_pins = $("#Pin_Numbers").val();
    var max_pins_for_column = 12;
    var number_of_columns = Math.ceil(number_of_pins/max_pins_for_column);
    var column_width = 100/number_of_columns; //width of the column in %
    var height_table = $('.window_container')[0].getBoundingClientRect().height;
    var tr_height = height_table/(max_pins_for_column+1);
    //Create the columns
    var columns_html = "";
    $(".column_pid_table").width("50%");
    var act_pin = 0;
    for (var i=1; i<=number_of_columns;i++){
        columns_html+= "<div class=\"column_pid_table\" style=\"width:" + column_width + "%\"\>";
        
        //Create the header for each column of the table
        columns_html+= "<table class=\"pin_table\" cellpadding=\"0\" cellspacing = \"0\">";
        columns_html+= "<tr style=\"height:" + tr_height + "px\">";
        columns_html+= "<td><label>" + "# Pin" + "</label></td>";
        columns_html+= "<td><label>" + "Name" + "</label></td>";
        columns_html+= "</tr>";
        
        for (var pin=act_pin+1; pin <= number_of_pins; pin++){
            columns_html+= "<tr style=\"height:" + tr_height + "px\">";
            columns_html+= "<td>";
            columns_html+= "<input id=\"Pin_ID_"+ pin +"\" value=\"" + pin + "\" class=\"Pin_table_input\" readonly=\"readoly\"/>";
            columns_html+= "</td>";
            columns_html+= "<td>";
            columns_html+= "<input id=\"Pin_Name_"+ pin +"\" value=\"" + pins_array[pin].name + "\" class=\"Pin_table_input pin_names\" readonly=\"readoly\"/>";
            columns_html+= "</td>";
            columns_html+= "</tr>";
            if(pin%max_pins_for_column == 0){
                act_pin = pin;
                break;
            }
        }
        
        columns_html+= "</table>";
        columns_html+= "</div>";
    }
    
    
    $("#pin_name_table").html(columns_html);
    Remove_Style_Classes();
    $(".Pin_table_input").addClass("element_ok");


    $(".Pin_table_input").click(function(event) {                
        Select_Pin_Row(event);
    });
    
    $(".pin_names").change(function (event){
        //Detect the id that has coused the event
        var id = (event.target.id).split("Pin_Name_")[1];
        Check_Pin_Name_Unicity(id);
    });

    
}

function Remove_Style_Classes(element){
    if(element == "all"){
        $(".Pin_table_input").removeClass("element_ok");
        $(".Pin_table_input").removeClass("element_incorrect");
        $(".Pin_table_input").removeClass("element_selected");
    }
    else{ //remove the classes only of the desired id pin
        $("#Pin_ID_"+element).removeClass("element_ok");
        $("#Pin_Name_"+element).removeClass("element_ok");
        $("#Pin_ID_"+element).removeClass("element_incorrect");
        $("#Pin_Name_"+element).removeClass("element_incorrect");
        $("#Pin_ID_"+element).removeClass("element_selected");
        $("#Pin_Name_"+element).removeClass("element_selected");
    }
    
}

function Select_Pin_Row(event){
    
    var id_select = event.target.id.split("Pin_")[1].split("_")[1];
    var id_selected_int = parseInt(id_select);
    Open_Window_Manage_Pin(id_selected_int);
    
}

function Open_Window_Manage_Pin(id_selected_pin){
    $(".Modal_Windows").css("visibility","hidden");
    $("#Window_Change_Pin_Settings").css("visibility","visible");
    $("#pin_name").html(id_selected_pin);
    
    Update_Pin_Settings_Arrows(id_selected_pin);
}

function Exit_Window_Change_Pin_Settings(){
    Show_Window_Manage_Pins("show");
    $("#right_icon_change_pin_settings").css("visibility","hidden"); 
    $("#left_icon_change_pin_settings").css("visibility","hidden"); 
}


function Update_Pin_Settings_Arrows(id_selected_pin){
    if(id_selected_pin == 1){
        $("#left_icon_change_pin_settings").css("visibility","hidden");        
    }
    else if(id_selected_pin == $("#Pin_Numbers").val()){
        $("#right_icon_change_pin_settings").css("visibility","hidden");
    }
    else{
        if($("#left_icon_change_pin_settings").css("visibility")=="hidden"){
             $("#left_icon_change_pin_settings").css("visibility","visible"); 
        }
        if($("#right_icon_change_pin_settings").css("visibility")=="hidden"){
            $("#right_icon_change_pin_settings").css("visibility","visible");
        }
    }
    //Assign to the input the name of the pin
    Load_Pin_Name(id_selected_pin);
    Check_Pin_Name_Unicity(id_selected_pin);
}

function Decrease_Pin_ID_Settings(){
    var id_selected_pin = $("#pin_name").html();
    id_selected_pin--;
    $("#pin_name").html(id_selected_pin);
    Update_Pin_Settings_Arrows(id_selected_pin);
    
}

function Increase_Pin_ID_Settings(){
    var id_selected_pin = $("#pin_name").html();
    id_selected_pin++;
    $("#pin_name").html(id_selected_pin);
    Update_Pin_Settings_Arrows(id_selected_pin);
}

function Load_Pin_Name(id){
    $("#setting_pin_name").val(pins_array[id].name);
}

function Mark_Pin_Name_Incorrect(){
    $("#setting_pin_name").removeClass("element_ok");
    $("#setting_pin_name").addClass("element_incorrect");
}

function Mark_Pin_Name_Correct(){
    $("#setting_pin_name").removeClass("element_incorrect");
    $("#setting_pin_name").addClass("element_ok");
}

function Check_Pin_Name_Unicity(id_selected_pin){
    var int_id_selected_pin = parseInt(id_selected_pin);
    var number_of_pins = $("#Pin_Numbers").val();
    //It is possible to change the name only if the row is highlighted, henge we have that the current pin number = old_id_selected
    var name_to_be_checked = $("#setting_pin_name").val();
    
    for(var i = 1; i<= number_of_pins; i++){
        if(i !== int_id_selected_pin){
            //Obtain the name of the pin i-th
            var name_comparison = $("#Pin_Name_" + i).val(); 
            //Compare it with the one I've changed
            if(name_to_be_checked == name_comparison){
                //If there is at least one pin with the same name, highlight it in red
                Mark_Pin_Name_Incorrect();
                $("#btn_Save_Pin_Settings").prop("disabled",true);
                break; //we can interrupt the for cycle
            }
            else{
                Mark_Pin_Name_Correct();
                $("#btn_Save_Pin_Settings").prop("disabled",false);
            }
        }
    }
}

function Save_Pin_Settings(id_selected_pin){
    var int_id_selected_pin = parseInt(id_selected_pin);
    //We save the properties of the object in the i-th position of the pins_array
    pins_array[int_id_selected_pin].name = $("#setting_pin_name").val();
    Exit_Window_Change_Pin_Settings("show");
}

function Draw_Symbol_Rectangle(){
    if($("#Pin_Numbers").val() % $("input[name = number_of_columns]:checked").val() !== 0){
        window.alert("error: impossible to have an odd number of pins in two columns");
        $("#svg_symbol").empty();
        return;
    }
    
    $("#svg_symbol").empty();
    var rect_width = 0;
    if($("input[name = number_of_columns]:checked").val() == 2){
        rect_width = rect_width_two_columns;
        $("#interaxis_label").css("visibility","visible");
        $("#Interaxis").css("visibility","visible");
    }
    else{
        rect_width = rect_width_one_column;
        $("#interaxis_label").css("visibility","hidden");
        $("#Interaxis").css("visibility","hidden");
    }
    var map = document.getElementById("svg_symbol");  
    var rect = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
        rect.setAttributeNS(null, 'x', $('.div_svg')[0].getBoundingClientRect().width/2 - $('.div_svg')[0].getBoundingClientRect().width/rect_width);
        rect.setAttributeNS(null, 'y', 0);
        rect.setAttributeNS(null, 'height', $('.div_svg')[0].getBoundingClientRect().height);
        rect.setAttributeNS(null, 'width', $('.div_svg')[0].getBoundingClientRect().width/rect_width*2);
        rect.setAttributeNS(null,'style','stroke:red; fill:none');
    map.appendChild(rect); 
    
    Draw_Symbol_Pins();
}

function Draw_Symbol_Pins(){    
    var number_of_pins = parseInt($("#Pin_Numbers").val());
    var columns        = parseInt($("input[name = number_of_columns]:checked").val());
    var rect_width     = 0;
    var columns        = columns;
    
    if(columns == 1){
        rect_width = rect_width_one_column;
        var actual_pin_radius = pin_radius/number_of_pins;
        var height_step = $('.div_svg')[0].getBoundingClientRect().height/(number_of_pins+1);
        var width_step  = ($('.div_svg')[0].getBoundingClientRect().width/rect_width*2)/3;
        for(var pin=1; pin<=number_of_pins; pin++){
            var x_center = ($('.div_svg')[0].getBoundingClientRect().width/2 - $('.div_svg')[0].getBoundingClientRect().width/rect_width) + 1*width_step;
            var y_center = height_step*pin;
            var map = document.getElementById("svg_symbol");
            var circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
                circle.setAttribute("cx", x_center);
                circle.setAttribute("cy", y_center);
                circle.setAttribute("r", actual_pin_radius);
                circle.setAttribute("fill", "none");
                circle.setAttribute("style", 'stroke:red; stroke-width:3');
                circle.setAttribute("id","pin_ID_" + pin);
            var xLine = document.createElementNS('http://www.w3.org/2000/svg', 'line');
                xLine.setAttribute('x1', x_center - width_step*2);
                xLine.setAttribute('y1', y_center);
                xLine.setAttribute('x2', x_center);
                xLine.setAttribute('y2', y_center);
                xLine.setAttribute('stroke', "red");
                xLine.setAttribute('stroke-width', 2);
            map.appendChild(xLine);
            map.appendChild(circle);
            $("#pin_ID_" + pin).val(pin);
            var x_text = document.createElementNS("http://www.w3.org/2000/svg", "text");
                x_text.setAttribute("x",x_center + width_step);
                x_text.setAttribute("y",y_center + Math.max(actual_pin_radius/2,5));
                x_text.setAttribute("font-size",Math.max(actual_pin_radius*2,10));
                x_text.innerHTML = pins_array[pin].name;
            map.appendChild(x_text);
        }         
    }
    else{
        rect_width = rect_width_two_columns;
        var actual_pin_radius = pin_radius/(number_of_pins/2);
        var height_step = $('.div_svg')[0].getBoundingClientRect().height/((number_of_pins/2)+1);
        var width_step  = ($('.div_svg')[0].getBoundingClientRect().width/rect_width*2)/6;
        
        //Left elements
        for(var pin=1; pin<=number_of_pins/2; pin++){
            var x_center = ($('.div_svg')[0].getBoundingClientRect().width/2 - $('.div_svg')[0].getBoundingClientRect().width/rect_width) + 1*width_step;
            var y_center = height_step*pin;
            var map = document.getElementById("svg_symbol");
            var circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
                circle.setAttribute("cx", x_center);
                circle.setAttribute("cy", y_center);
                circle.setAttribute("r", actual_pin_radius);
                circle.setAttribute("fill", "none");
                circle.setAttribute("style", 'stroke:red; stroke-width:3');
                circle.setAttribute("id","pin_ID_" + pin);
            var xLine = document.createElementNS('http://www.w3.org/2000/svg', 'line');
                xLine.setAttribute('x1', x_center - width_step*2);
                xLine.setAttribute('y1', y_center);
                xLine.setAttribute('x2', x_center);
                xLine.setAttribute('y2', y_center);
                xLine.setAttribute('stroke', "red");
                xLine.setAttribute('stroke-width', 2);
            map.appendChild(xLine);
            map.appendChild(circle);
            $("#pin_ID_" + pin).val(pin);
            var x_text = document.createElementNS("http://www.w3.org/2000/svg", "text");
                x_text.setAttribute("x",x_center + width_step);
                x_text.setAttribute("y",y_center + Math.max(actual_pin_radius/2,5));
                x_text.setAttribute("font-size",Math.max(actual_pin_radius*2,10));
                x_text.innerHTML = pins_array[pin].name;
            map.appendChild(x_text);     
        }
        //Right elements
        for(var pin=1; pin<=number_of_pins/2; pin++){
        var x_center = ($('.div_svg')[0].getBoundingClientRect().width/2 - $('.div_svg')[0].getBoundingClientRect().width/rect_width) + 5*width_step;
        var y_center = height_step*pin;
        var map = document.getElementById("svg_symbol");
        var circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
            circle.setAttribute("cx", x_center);
            circle.setAttribute("cy", y_center);
            circle.setAttribute("r", actual_pin_radius);
            circle.setAttribute("fill", "none");
            circle.setAttribute("style", 'stroke:red; stroke-width:3');
            circle.setAttribute("id","pin_ID_" + (pin + number_of_pins/2));
        var xLine = document.createElementNS('http://www.w3.org/2000/svg', 'line');
            xLine.setAttribute('x1', x_center + width_step*2);
            xLine.setAttribute('y1', y_center);
            xLine.setAttribute('x2', x_center);
            xLine.setAttribute('y2', y_center);
            xLine.setAttribute('stroke', "red");
            xLine.setAttribute('stroke-width', 2);
        map.appendChild(xLine);
        map.appendChild(circle);
        $("#pin_ID_" + (pin + number_of_pins/2)).val((pin + number_of_pins/2));
        var x_text = document.createElementNS("http://www.w3.org/2000/svg", "text");
            x_text.setAttribute("x",x_center - width_step - actual_pin_radius);
            x_text.setAttribute("y",y_center + Math.max(actual_pin_radius/2,5));
            x_text.setAttribute("font-size",Math.max(actual_pin_radius*2,10));
            x_text.innerHTML = pins_array[pin + number_of_pins/2].name;
        map.appendChild(x_text);  
        }    
    }    
}

function Change_Number_Of_Columns()
{    
    Draw_Symbol_Rectangle();
}

function Generate_Pin_Array(){
    var number_of_pins = $("#Pin_Numbers").val();
    pins_array = new Array();
    for (var i = 1; i<= number_of_pins; i++){
        var pin_object = new Object();
            pin_object.name = i;
            pin_object.visibility = "true"; //if true the pin is visible on the board,
            pin_object.hole       = "true"; //if the pin is used the hole will be done automatically, if it is not used, hence visibility = false, then the pin can have or not the hole in order to preserve mountability properties
        pins_array[i] = pin_object;
    }
    //pins_array_backup = pins_array;
    Draw_Symbol_Rectangle();
}